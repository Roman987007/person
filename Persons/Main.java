package ru.roman.Person.Persons;

import java.lang.reflect.Array;
import java.util.*;

/**
 * Класс вывода данным о персонах.
 */
public class
Main {
    public static void main(String[] args) {

        Student NikitaYidin = new Student("Никита Юдин", Gender.MAN, 1998, 2014, "09.02.01");
        Student AndreyRazhkin = new Student("Андрей Рыжкин", Gender.MAN, 1998, 2014, "09.02.01");
        Student AndreyKhozin = new Student("Андрей Хозин", Gender.MAN, 1998, 2014, "09.02.01");
        Student DmitriyKyzmin = new Student("Дмитрий Кузьмин", Gender.MAN, 1998, 2014, "09.02.01");
        Student AlexandraPopova = new Student("Александра Попова", Gender.WOMEN, 1998, 2014, "09.02.01");
        Student TanyaSkvortsova = new Student("Таня Скворцова", Gender.WOMEN, 1998, 2014, "09.02.01");

        Professor ITA = new Professor("Инюшкина Татьяна", Gender.WOMEN, 1965, "Программирование", false);
        Professor Novomlincev = new Professor("Новомлинцев Антон", Gender.MAN, 1991, "Архитектура", false);
        Professor Tarasova = new Professor("Тарасова Лариса", Gender.WOMEN, 1962, "ВРПП", true);
        Professor Tokmakova = new Professor("Токмакова Людмила", Gender.WOMEN, 1970, "История", false);
        Professor Dorofeev = new Professor("Дорофеев Дмитрий", Gender.MAN, 1985, "Физика", false);

        Student student[] = {NikitaYidin, AndreyRazhkin, AndreyKhozin, DmitriyKyzmin, AlexandraPopova, TanyaSkvortsova};
        Professor professor[]={ITA, Novomlincev, Tarasova, Tokmakova, Dorofeev};

       ArrayList<Student>studentsman = new ArrayList<Student>();
       ArrayList<Professor>professorsman = new ArrayList<Professor>();

        System.out.println("Студенты мужского пола: ");
       for (int i = 0; i < student.length; i++) {
            if (student[i].getGender() == Gender.MAN) studentsman.add(student[i]);
        }

        for (int i = 0; i < studentsman.size(); i++ )
        {
            System.out.println(studentsman.get(i).getSurname());
        }
        System.out.println();

        System.out.println("Учителя мужского пола: ");
        for (int i = 0; i <professor.length; i++){
            if (professor[i].getGender() == Gender.MAN) professorsman.add(professor[i]);
        }
        for (int i = 0; i <professorsman.size(); i++){
            System.out.println(professorsman.get(i).getSurname());
        }

    }

}
