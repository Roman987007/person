package ru.roman.Person.Persons;

import java.util.Scanner;

/**
 * Класс информация о персоне.
 * @author Р. Петрушов.
 * @version 1.0
 * @since 16.06.2016
 */
public class Person {
    /*
    *surname - имя персоны.
    * gender - пол персоны.
    * YearOFBirth - год рождения персоны.
     */
    private String surname;
    private Gender gender;
    private int YearOfBirth;

    public Person() {
        surname = "Имя, Фамилия";
        YearOfBirth = 0;
        gender = Gender.NO_GENDER;

    }

    public Person(String surname, Gender gender, int yearOfBirth) {
        this.surname = surname;
        this.gender = gender;
        YearOfBirth = yearOfBirth;
    }

    public String getSurname() {
        return surname;
    }

    public Gender getGender() {
        return gender;
    }

    @Override
    public String toString() {
        return super.toString()+
                "Person{" +
                "surname='" + surname + '\'' +
                ", gender=" + gender +
                ", YearOfBirth=" + YearOfBirth +
                '}';
    }
    public void inputPerson() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите Имя,Фамилия: ");
        setSurname(scanner.next());

        System.out.print("Введите ваш пол: ");
        String gender = scanner.next();
        gender.toLowerCase();
        System.out.print(gender);
        if (gender.equals("MAN")) ;
    }

    public int getYearOfBirth() {
        return YearOfBirth;
    }

    public void setSurname(String surname) {
        this.surname = surname;
    }

    public void setGender(Gender gender) {
        this.gender = gender;
    }

}
