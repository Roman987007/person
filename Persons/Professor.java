package ru.roman.Person.Persons;

import java.util.Scanner;

/**
 * Класс информация об учителях.
 * @author Р. Петрушов.
 * @version 1.0
 * @since 16.06.2016
 */
public class Professor extends Person {
    /*
    *descipline - название дисциплины, которую преподает преподаватель.
    * curator - переменная, которая узнает является ли преподаватель куратором или нет.
     */
    private String descipline;
    private boolean curator;

    public Professor() {
        super("Name", Gender.NO_GENDER, 0);
        descipline = "Example";
        curator = false;

    }

    public Professor(String surname, Gender gender, int yearOfBirth, String descipline, boolean curator) {
        super(surname, gender, yearOfBirth);
        this.descipline = descipline;
        this.curator = curator;
    }

    public String getDescipline() {
        return descipline;
    }

    public boolean isCurator() {
        return curator;
    }

    @Override
    public String toString() {
        return super.toString() +
                "Professor{" +
                "descipline='" + descipline + '\'' +
                ", curator=" + curator +
                '}';
    }
    public void inputProfessor() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите Имя: ");
        setSurname(scanner.next());

        System.out.print("Введите ваш пол: ");
        String gender = scanner.next();
        gender.toLowerCase();
        System.out.print(gender);
        if (gender.equals("MAN")) ;


        System.out.print("Введите название дисциплины: ");
        setDescipline(scanner.next());

        System.out.print("Куратор или нет?: ");
        setCurator(scanner.nextBoolean());
    }


    public void setDescipline(String descipline) {
        this.descipline = descipline;
    }

    public void setCurator(boolean curator) {
        this.curator = curator;
    }
}
