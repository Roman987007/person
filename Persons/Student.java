package ru.roman.Person.Persons;

import java.util.Scanner;

 /**
 * Класс информация о студентах.
 * @author Р. Петрушов.
 * @version 1.0
 * @since 16.06.2016
 */
public class Student extends Person {
    /*
    *YearOfStart - год поступления студента.
    * CodeOFSpecialty - код специальности студента.
     */
    private int YearOfStart;
    private String CodeOfSpecialty;

    public Student() {
        super("Name", Gender.NO_GENDER,0);
        YearOfStart = 0;
        CodeOfSpecialty = "00.00.00";
    }

    public Student(String surname, Gender gender, int yearOfBirth, int yearOfStart, String codeOfSpecialty) {
        super(surname, gender, yearOfBirth);
        YearOfStart = yearOfStart;
        CodeOfSpecialty = codeOfSpecialty;
    }

    @Override
    public String toString() {
        return super.toString()+
                "Student{" +
                "YearOfStart=" + YearOfStart +
                ", CodeOfSpecialty='" + CodeOfSpecialty + '\'' +
                '}';
    }
    public void inputStudent() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Введите Имя: ");
        setSurname(scanner.next());

        System.out.print("Введите ваш пол: ");
        String gender = scanner.next();
        gender.toLowerCase();
        System.out.print(gender);
        if (gender.equals("MAN"));


        System.out.print("Введите год рождения: ");
        setYearOfBirth(scanner.next());

        System.out.print("Введите код специальности: ");
        CodeOfSpecialty = scanner.next();


        System.out.print("Введите год поступления: ");
        YearOfStart = scanner.nextInt();

    }


    public int getYearOfStart() {
        return YearOfStart;
    }

    public String getCodeOfSpecialty() {
        return CodeOfSpecialty;
    }

    public void setYearOfBirth(String yearOfBirth) {
        yearOfBirth = yearOfBirth;
    }

}